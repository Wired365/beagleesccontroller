import os
import struct
from sys import argv

# check to make sure the proper number of motor commands was passed
if (len(argv) != 5):
    print "Wrong number of input commands, script accepts exactly 4 commands"
    exit()

# get motor commands (input commands should be defined  in microseconds)
script, motor1, motor2, motor3, motor4 = argv

# Multiplier to convert microseconds to control loop cycles, base on a 50 nS loop
loop_multiplier = 20

# Convert the uS pulse time into cyles of the control loop
command1 = int(motor1) * loop_multiplier
command2 = int(motor2) * loop_multiplier
command3 = int(motor3) * loop_multiplier
command4 = int(motor4) * loop_multiplier

# Convert the motor commands to a byte stream for writing
data = struct.pack("IIII", command1, command2, command3, command4)

# wrtie the motor commands to the PRUs
dev = os.open("/dev/rpmsg_pru31", os.O_RDWR)
os.write(dev, data)
os.close(dev)
