// Firmware for PRU1

#include <stdint.h>
#include <stdio.h>
#include <pru_rpmsg.h>
#include <rsc_types.h>
#include <pru_cfg.h>
#include <pru_intc.h>

#include "resource_table_1.h"

volatile register uint32_t __R30;
volatile register uint32_t __R31;

// Host Interrupt sets bit 30 of R31 (p.26 of PRU Reference)
#define HOST_INT ((uint32_t)1 << 31)

// defines the system events 18 (to ARM) and 19 (from ARM) for PRU1
#define TO_ARM_HOST 18
#define FROM_ARM_HOST 19

// Using the name 'rpmesg-pru' will probe rpmsg_pru driver found at /drivers/rpmsg/rpmsg_pru.c
// Use channel 31 for the device driver.
#define CHAN_NAME "rpmsg-pru"
#define CHAN_DESC "Channel 31"
#define CHAN_PORT 31

// Used to make sure the Linux drivers are ready for RPMsg communication
// Found at /include/uapi/linux/virtio_config.h
#define VIRTIO_CONFIG_S_DRIVER_OK 4

// 16 bytes are used to transfer the motor command data (4 bytes per register and 4 registers)
//uint8_t paylod[16]
uint8_t payload[RPMSG_BUF_SIZE];

// Memory address of the shared RAM
#define PRU_SHAREDMEM 0x00010000
#define SP_BANK_0 10
#define DATA_START_REGISTER_NUMBER 10
#define MOTOR_DATA_SIZE 16


// Sets up the registers to store the motor commands in
volatile uint32_t __R10;
volatile uint32_t __R11;
volatile uint32_t __R12;
volatile uint32_t __R13;
uint32_t data_size = 16;
uint8_t data_array[16];


void main()
{
    // Set the values in scratch space to the lowest command for motors (to turn them on)
    int ind;
    for (ind = 0; ind < data_size; ind+=4)
    {
        data_array[ind+3] = 0x00;
        data_array[ind+2] = 0x00;
        data_array[ind+1] = 0x3E;
        data_array[ind] = 0x80;
    }
    
     __xout(SP_BANK_0, DATA_START_REGISTER_NUMBER, 0, data_array);
    
    // Setup all variabls necessary for RPMsg
    struct pru_rpmsg_transport transport;
    uint16_t source;
    uint16_t dest;
    uint16_t length;
    volatile uint8_t *status;

    /* NOTE: All CT_CFG and CT_INTC variables defined in pru_cfg.h and pru_intc.h*/

    // Allows the PRU access to external memories via the OCP master port
    CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

    // Clear the status of the PRU-ICSS system event that the ARM will use to 'kick' the PRU
    CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;

    // Ensure the Linux drivers are ready for RPMsg communication
    status = &resourceTable.rpmsg_vdev.status;
    while (!(*status & VIRTIO_CONFIG_S_DRIVER_OK))
    {
    }

    // Initialize the RPMsg transport structure.
    pru_rpmsg_init(&transport, &resourceTable.rpmsg_vring0, &resourceTable.rpmsg_vring1, TO_ARM_HOST, FROM_ARM_HOST);

    // Block until the channel is established
    while (pru_rpmsg_channel(RPMSG_NS_CREATE, &transport, CHAN_NAME, CHAN_DESC, CHAN_PORT) != PRU_RPMSG_SUCCESS)
    {
    }

    // Infinite loop for processing incoming messages
        // PRU will read new message from ARM and write the corresponding motor commands to bank0 for PRU0 to use
        // Motor commands should be number of 'time loops' that PRU0 must execute to get the desired high pulse length
    while (1)
    {
        if (__R31 & HOST_INT)
        {
            // Clear the event status after receiving an interrupt
            CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;

            // Receive all available messages, multiple messages can be sent per interrupt
            while (pru_rpmsg_receive(&transport, &source, &dest, payload, &length) == PRU_RPMSG_SUCCESS)
            {
                // Reflect the received message for record keeping
                pru_rpmsg_send(&transport, dest, source, payload, length);

                // Move relevant data from the incoming message into a local data array for writing to scratch space
                for (ind = 0; ind < data_size; ++ind)
                {
                    data_array[ind] = payload[ind];                    
                }

                // write all register values to scratch pad for PRU0 to use.
                // 10 = Bank0, no remapping is used and 16 bytes in R10-R13 are written
                __xout(SP_BANK_0, DATA_START_REGISTER_NUMBER, 0, data_array);

            }
        }
    }

}
