; PRU0 Firmware Assembly Code
; Sets up PRU0 to drive the motors with precise timing
;.origin 0
;.entrypoint START

    .cdecls "main_pru0.c"

    .global main

main:
start:
    LDI32 R30, 0x00000000
    LDI32 R31, 0x00000000   ; Set all output pins to low on startup

load_pulse:
    XIN 10, &R10, 16    ; Loads all motor loops counts form scratch pad
    LDI32 R0, 0x00061A80    ; Pulse Loop Number Period (50 Hz) for 50ns loop

set_bits:
    SET R30.b0, R30.b0, 0
    SET R30.b0, R30.b0, 1
    SET R30.b0, R30.b0, 2
    SET R30.b0, R30.b0, 3

; Loop with 4 motors takes 50 nS (10 instructions) per full loop. Ignoring minor variances of clearing
pulse_loop:
    SUB R0, R0, 1
    SUB R10, R10, 1
    SUB R11, R11, 1
    SUB R12, R12, 1
    SUB R13, R13, 1

    QBNE skip1, R10, 0
    CLR R30.b0, R30.b0, 0
    ; Bit check should use QBNE for all output bits, only pulse width (r0) uses QBEQ
skip1:
    QBNE skip2, R11, 0
    CLR R30.b0, R30.b0, 1

skip2:
    QBNE skip3, R12, 0
    CLR R30.b0, R30.b0, 2

skip3:
    QBNE skip4, R13, 0
    CLR R30.b0, R30.b0, 3

skip4:
    QBNE pulse_loop, R0, 0
    QBA load_pulse
    HALT

