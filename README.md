# Beagle ESC Controller README #

The above repo contains the bare minimum code for driving an ESC directly from the Beaglebone using the sub real time processing units known as the PRUs.
The code is a cleaned up version taken from a seperate project. I will continue to update the documentation and code as I have time.
This code is provided free of charge and in doing so I do not take any responsiblity for any damages hardware or software related that could result from it's use or missuse.

## Repository Details ##

	* Version 1.0
	* Last Updated: 29 January 2017

## How do I get set up? ##
### Assumptions ###

	* I currently assume that you have updated your OS to the most recent version of debian as well as the kernel.
	* I assume that you have installed the pru-software-support-package

### Step-by-Step Setup ###

The following steps are for setting up the repo only. I will eventually add more information on setting up all of the dependencies in the future.

	1. Clone the repository
	2. Run make on the enclosed makefile to build firmware for both PRUs
		* The root path to the pru-software-support-package may need to be redefined in the makefile if your installation differs from mine.
	3. Once the PRU firmware has compiled either use the install command built into the makefile or copy the firmware yourself to /lib/firmware/PRU_FIRMWARE_NAME
		* If copying manually make sure you use the correct firmware names. If alternative names are used then the PRU loader will not load the correct software on startup of the PRUs.
	4. Ensure that the ESC's signal wires are connected to the correct pins.
		1. ESC 1 = P9_31
		2. ESC 2 = P9_29
		3. ESC 3 = P9_30
		4. ESC 4 = P9_28
	5. Run "setup_pru_pins" to enable the PRU control of the 4 header pins.
		* Make sure no signals are being driven into the above pins as it could cause a problem for the board, or worse case damage it.
		* If you need less than 4 output motors, feel free to modify the "setup_pru_pins" script as you see fit.
		* This script needs to be run everytime power is cycled to the Beaglebone NOT everytime the firmware changes
	6. Start the PRUs using the "start" and "stop" bash scripts included with the repo.
		* If you decided to alter the firmware in any way these scripts will be what start/stops the PRUs as well as loads the firmware.
		* PRUs must be stopped and started for new firmware to take effect, the Beaglebone does NOT need to be power cycled.
	7. Use the "motor.py" script to send a set of 4 motor commands to the connected ESCs.
		* Remember that the commands must be in number of microseconds.
			* EX: "motor 800 800 800 800" = 800 microsecond high pulse for all 4 connected motors.
		
## Resources ##

	* Link to the official elinux page regarding the PRUs: http://elinux.org/EBC_Exercise_30_PRU_via_remoteproc_and_RPMsg
	* Link to the TI PRU support package: https://git.ti.com/pru-software-support-package
	* Link to fellow forum posters project detailing his setup and use of the PRUs: https://github.com/Greg-R/pruadc1
		* For full details on his setup and the details of his project please see the following documentation: https://github.com/Greg-R/pruadc1/blob/master/doc/PRUADC1latex/PRUADC1.pdf

## Issues & Questions ##
	* If you have found a bug in the code please submit it using the issue tracker, I will do my best to fix them as time permits.